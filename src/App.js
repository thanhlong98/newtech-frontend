import React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import './scss/style.css'
import Admin from './views/Admin/Admin';
import Login from './views/LogIn/LogIn';

function App() {
  return (
    <HashRouter>
          <Switch>
            <Route exact path="/admin" name="admin" component={Admin} />
            <Route path="/" name="Login" component={Login} />
           
          </Switch>
      </HashRouter>
  );
}

export default App;
