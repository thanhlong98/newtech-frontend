//const API_URL = "http://3.137.153.185/";
const API_URL = "http://localhost:5000/"

const POST_LOGIN = API_URL + 'login';
const GET_ALL_USER = API_URL + 'admin/users';
const DELETE_USER = API_URL + 'admin/users/'
const RESET_USER_PASSWORD = API_URL + 'admin/users/reset/'

export const API_CONST = {
    API_URL,
    POST_LOGIN,
    GET_ALL_USER,
    DELETE_USER,
    RESET_USER_PASSWORD

}