import { API_CONST } from './API'
const user_token = localStorage.getItem('auth-token') ? localStorage.getItem('auth-token') : ' '
const login = async(user_id, password) => {

    let res;
    let op = {
        method: "POST",
        body: JSON.stringify({
            "user_id": user_id,
            "password": password
        }),
        headers: {
            'Content-Type': 'Application/json',
            'auth-token': user_token,
        }
    }
    let url = API_CONST.POST_LOGIN;
    try {
        res = await fetch(url, op)
        let body = await res.json()
        return [true, body]
            /* [{errorCode : 0},
                   body:{
                       "message": "logged in",
                       "accessToken":"day la cai access token"
                   }]*/
    } catch (err) {
        if (res && res.statusText) {

            console.log(res.statusText)
            return [false, null]
        } else {
            console.log("loi roi")
            return [false, null]
        }

    }
};
const resetUserPassword = async(user_id) => {
    let res
    let op = {
        method: 'POST',
        headers: {
            'Content-Type': 'Application/json',
            'auth-token': user_token,
        }
    }
    let url = API_CONST.RESET_USER_PASSWORD + user_id
    try {
        res = await fetch(url, op)
        let body = await res.json()
        if (body.errorCode === 200) {
            return true
        }
    } catch (error) {
        return false
    }
}
const deleteUser = async(user_id) => {
    let res
    let op = {
        method: 'DELETE',
        headers: {
            'Content-Type': 'Application/json',
            'auth-token': user_token,
        }
    }
    let url = API_CONST.DELETE_USER + user_id
    try {
        res = await fetch(url, op)
        let body = await res.json()
        if (body.errorCode === 200) {
            return true
        }
    } catch (error) {

    }
}
const getAllUser = async() => {
    let res;
    let op = {
        method: "GET",
        headers: {
            'Content-Type': 'Application/json',
            'auth-token': user_token,
        }
    }
    let url = API_CONST.GET_ALL_USER;
    try {
        res = await fetch(url, op)
        let body = await res.json()
        if (body.errorCode === 401 || body.errorCode === 404) {
            return [false, null];
        }
        return [true, body]
            /* [{errorCode : 0},
                   body:{
                       "message": "logged in",
                       "accessToken":"day la cai access token"
                   }]*/
    } catch (err) {
        if (res.errorCode) {
            console.log(res.errorCode)
        } else {
            console.log("loi roi")
            return [false, null]
        }
    }
}

export const UserService = {
    login,
    getAllUser,
    deleteUser,
    resetUserPassword
}