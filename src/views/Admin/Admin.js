import React, { Component } from 'react';
// import { Button } from 'react-bootstrap';
import Table from 'react-bootstrap/Table'
import {Redirect} from 'react-router-dom'
// import AdminTable from '../Admin/AdminTable'
import { UserService } from '../Service/UserService'
import AdminItem from './AdminItem'

class Admin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allUser: [],
            isLogout: false,
            isAuthenticate: true
        };
    }

    componentDidMount() {
        this.getAllUser();
    }
    onDeleteUser = async (user_id) => {
        const success = await UserService.deleteUser(user_id)
        if (success) {
            let prevState = []
            prevState = this.state.allUser
            let newState = prevState.map((user) => {
                if (user && user.props.user.user_id !== user_id) return user
            })
            this.setState({ allUser: newState })
        }
    }
    onResetUserPassword = async (user_id)=>{
        const success = await UserService.resetUserPassword(user_id)
        if(success){
            console.log('email sent')
        }
    }
    getAllUser = async () => {
        const [success, allUser] = await UserService.getAllUser()
        if (success) {
            const all = allUser.map(user => {
                return (
                    <AdminItem user={user} key={user.user_id} onDeleteUser={this.onDeleteUser.bind(this)}
                    onResetUserPassword={this.onResetUserPassword.bind(this)}></AdminItem>
                )
            })
            this.setState({
                allUser: all,
                isAuthenticate:true
            })
        }
        if(!success)
        this.setState({isAuthenticate:false})
      //  window.location.href = "#/login";

    }
    logout = () =>{
        localStorage.setItem('auth-token','');
        this.setState({isLogout:true})
    }

    render() {
        if (this.state.isLogout) return <Redirect to ="/login"/>
        if( !this.state.isAuthenticate) return <Redirect to ='/login'/>
        if(localStorage.getItem('auth-token').length === 0 ) return <Redirect to = "/login"/>
        return (
            <div>
                <div className="admin-page">
                    <div className="welcom-admin">WelCom Admin</div>
                    <div className="log-out">
                        <button className="button-log" onClick={this.logout}>Đăng Xuất</button>
                    </div>
                    
                </div>
                <div className="table-admin">
                    <Table striped bordered hover >
                        <thead>
                            <tr>
                                <th >User Id</th>
                                <th>User Name</th>
                                <th>Phone Number</th>
                                <th>Email</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        {this.state.allUser}
                    </Table>
                </div>
                
            </div>

        );

    }
}

export default Admin;