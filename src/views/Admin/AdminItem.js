import React, { Component } from 'react';
import {Button} from 'react-bootstrap'



class AdminItem extends Component {
    constructor(props){
        super(props)
        this.state={}
    }
    deleteUser(user_id){
        this.props.onDeleteUser(this.props.user.user_id)
    }
    resetUserPassword(user_id){
        this.props.onResetUserPassword(this.props.user.user_id)
    }
    render() {
        return (
            <tbody>
                <tr>
                    <td>{this.props.user.user_id}</td>
                    <td>{this.props.user.name}</td>
                    <td>{this.props.user.phone_number}</td>
                    <td>{this.props.user.email}</td>
                    <td><Button variant="outline-warning" onClick={()=>this.resetUserPassword(this.props.user.user_id)}>reset pass</Button></td>
                    <td><Button variant="outline-danger" onClick={()=>{this.deleteUser(this.props.user.user_id)}}>Delete</Button>{' '}</td>
                    </tr>
            </tbody>
        );
    }
}

export default AdminItem;