import React from 'react';
import {Input} from "reactstrap";
import {Link,Redirect} from "react-router-dom"
import {UserService} from '../Service/UserService';
class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            password: '',
            isSubmitting: false,
            phoneNumber:'',
            error: false,
            isAuthenticate: false
        }
    }

    handleSubmit = async(event)=>{
        //event.preventDefault();
        if(this.state.phoneNumber.length ===0 || this.state.password.length===0) {
            this.setState({error:true})
        }
        if(this.state.phoneNumber && this.state.password){
            this.setState({isSubmitting:true})
            let [success,body]= await UserService.login(this.state.phoneNumber,this.state.password)
            if(success && body.errorCode === 200){
                this.setState({isSubmitting:false})
                let token = body.accessToken
                localStorage.setItem('auth-token',token)
                this.setState({isAuthenticate:true})
            }
        }
        
    }
    handlePhoneNumber = (event) => {
        this.setState({phoneNumber: event.target.value, errorPhoneNumber: false})
    };

    handlePassword = (event) => {
        this.setState({password: event.target.value, errorPassword:false})
    };
    render() {
        const {error, isSubmitting} = this.state;
        if (this.state.isAuthenticate) return <Redirect to='/admin'/>
        return (
            <div className='login-background'>
                <div className='login-content'>
                    <p className='header-text'>Đăng nhập tài khoản Admin</p>
                    <form onSubmit={this.handleSubmit}>
                        <div className='input-login-content'>
                        <Input
                            className='input-login'
                            placeholder='Tên đăng nhập'
                            value={this.state.phoneNumber}
                            onChange={this.handlePhoneNumber}
                        />
                        { error &&
                            <div className='error-text'>Hãy nhập số điện thoại</div>
                        }
                        </div>
                        <div className='input-login-content input-password'>
                        <Input
                            type='password'
                            className='input-login'
                            placeholder='Mật khẩu'
                            value={this.state.password}
                            onChange={this.handlePassword}
                        />
                            { error &&
                            <div className='error-text'>Hãy nhập password</div>
                            }
                        </div>
                        <a  className="forget-password" href="#/forget-password">Quên mật khẩu</a><br/>
                        <button className='button-submit' disabled={isSubmitting} onClick={this.login}>ĐĂNG NHẬP</button>
                    </form>
                </div>
                <div className="footer-ims">
            </div>
            </div>
        )
    }

}
export default Login;